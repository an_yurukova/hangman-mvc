<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Hangman</title>
<link rel="stylesheet"
	href="/webjars/bootstrap/4.5.3/css/bootstrap.min.css">
</head>
<body>
	<div
		style="padding: 1%; background-color: #000080; color: white; font-weight: bold; font-family: verdana;">Hangman</div>

	<div style="padding: 1%; background-color: #ffd480;">
		<span style="padding-left: 90%; font-weight: bold">Tries left:
			${lifes}</span>

		<h2 class="font-weight-bold p-3 text-center text-uppercase">${word}</h2>
		<br>
		<div class="text-center">
			<c:forEach items="${alphabet}" var="letter">
				<%-- <form method="POST" name="letter" action="/games/${id}/tries" --%>
				<c:if test="${not fn:contains ((game.letters), letter)}">

					<form method="POST" action="/games/${id}/tries"
						style="display: inline;">
						<input
							class="btn btn-primary border-0 font-weight-bold text-uppercase"
							type="submit" name="letter" value="${letter}"></input>
					</form>
				</c:if>
			</c:forEach>
		</div>

	</div>
	<script rel="stylesheet" src="/webjars/jquery/3.5.1/jquery.min.js"></script>
	<script rel="stylesheet"
		src="/webjars/bootstrap/4.5.3/js/bootstrap.min.js"></script>
	<script src='https://kit.fontawesome.com/a076d05399.js'></script>

</body>
</html>