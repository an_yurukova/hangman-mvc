<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Game over</title>
<link rel="stylesheet"
	href="/webjars/bootstrap/4.5.3/css/bootstrap.min.css">
</head>
<body>
	<div
		style="padding: 1%; background-color: #000080; color: white; font-weight: bold; font-family: verdana;">Hangman</div>
	<div style="padding: 1%;">
		<c:choose>
			<c:when test="${message=='Well done!'}">
				<div class="container text-center">
					<br />
					<div class="alert alert-success" role="alert">
						<strong>${message}</strong>
					</div>
					<br />
				</div>
			</c:when>
			<c:otherwise>
				<div class="container text-center">
					<br />
					<div class="alert alert-danger" role="alert">
						<strong>${message}</strong>
					</div>
					<br />
				</div>
			</c:otherwise>
		</c:choose>
		<br />
		<form method="POST" action="/games">
			<div class="row justify-content-center">
				<button type="submit" class="btn btn-primary">Start new
					game</button>
			</div>
		</form>
	</div>
</body>
</html>