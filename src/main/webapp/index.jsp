<html>
<link rel="stylesheet"
	href="/webjars/bootstrap/4.5.3/css/bootstrap.min.css">
<body>
	<div
		style="padding: 1%; background-color: #000080; color: white; font-weight: bold; font-family: verdana;">Hangman</div>
	<div style="padding: 1%; background-color: #ffd480;">
		<h2 class="font-weight-bold text-center">Welcome</h2>
		<br>


		<div class="row justify-content-center">
			<form method="POST" action="/games">
				<button type="submit" class="btn btn-primary">Start new
					game</button>
			</form>
		</div>

	</div>
</body>
<script rel="stylesheet" src="/webjars/jquery/3.5.1/jquery.min.js"></script>
<script rel="stylesheet"
	src="/webjars/bootstrap/4.5.3/js/bootstrap.min.js"></script>
</html>
