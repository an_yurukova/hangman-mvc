package bg.sissi.hangman.web;

import bg.sissi.hangman.model.Game;

public interface GameService {

  Game startNewGame();

  Game getGameById(String id);

  void checkLetter(String id, String letter);
}