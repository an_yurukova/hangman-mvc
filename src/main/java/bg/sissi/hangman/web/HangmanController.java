package bg.sissi.hangman.web;

import bg.sissi.hangman.model.Game;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/games")
public class HangmanController {

  private static final String[] ALPHABET =
      IntStream.range('a', 'z' + 1)
          .mapToObj(c -> Character.toString((char) c))
          .toArray(String[]::new);

  @Autowired private GameService gameService;

  @PostMapping
  public String createGame() {
    String id = gameService.startNewGame().getId();
    return "redirect:/games/" + id;
  }

  @GetMapping("/{id}")
  public String showStartPage(@PathVariable String id, Model model) {
    Game game = gameService.getGameById(id);

    String letters =
        game.getLetters().stream().map(c -> c.toString()).collect(Collectors.joining(""));

    if (letters.isEmpty()) {
      model.addAttribute("word", game.getWord().replaceAll("[a-z]", "_"));
    } else {
      model.addAttribute("word", game.getWord().replaceAll("[^" + letters + "]", "_"));
    }
    int lifes = game.getLifes();

    if (!((String) model.getAttribute("word")).contains("_")) {

      setMessage(model, "Well done!");
      return "game-over";
    } else if (lifes == 0) {
      setMessage(model, "Sorry.");
      return "game-over";
    } else {

      model.addAttribute("game", game);
      model.addAttribute("id", id);
      model.addAttribute("lifes", lifes);
      return "display-game";
    }
  }

  @PostMapping("/{id}/tries")
  public String checkIfLetterIsPresent(@PathVariable String id, @RequestParam String letter) {
    gameService.checkLetter(id, letter);
    return "redirect:/games/" + id;
  }

  @GetMapping(path = "/{id}/real-word")
  public @ResponseBody String showTheWord(@PathVariable String id) {
    Game game = gameService.getGameById(id);
    return game.getWord();
  }

  private void setMessage(Model model, String message) {
    model.addAttribute("message", message);
  }

  @ModelAttribute("alphabet")
  public String[] alphabet() {
    return ALPHABET;
  }
}
