package bg.sissi.hangman.web;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GeneralExceptionHandler {

  @ExceptionHandler(value = Exception.class)
  public String handleException() {
    return "exception";
  }
}
