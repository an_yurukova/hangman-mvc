package bg.sissi.hangman.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatObject;
import bg.sissi.hangman.model.Game;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class GameServiceTest {

  @Spy GameService gameServ = new GameServiceImpl();

  @Test
  public void testGameCreation() {
    GameService gameService = new GameServiceImpl();
    Game game = gameService.startNewGame();
    assertThatObject(game).isEqualTo(gameService.getGameById(game.getId()));
  }

  @Test
  public void whenLetterIsNotPresent_thenLifesAreDecreased() {
    Game gameDummy = new Game("table");
    Mockito.when(gameServ.startNewGame()).thenReturn(gameDummy);
    Mockito.when(gameServ.getGameById(Mockito.anyString()))
        .thenReturn(gameDummy, gameDummy, new Game("tiger"));
    Game game = gameServ.startNewGame();

    assertThat(game.getLifes()).isEqualTo(8);

    gameServ.checkLetter(game.getId(), "z");

    assertThat(game.getLifes()).isEqualTo(7);

    assertThat(game.getLetters()).contains('z');

    gameServ.checkLetter(game.getId(), "d");

    assertThat(game.getLifes()).isEqualTo(6);

    gameServ.checkLetter(game.getId(), "a");

    assertThat(game.getLifes()).isEqualTo(6);

    assertThat(game.getLetters()).doesNotContain('a');

    gameServ.checkLetter(game.getId(), "z");

    assertThat(game.getLifes()).isEqualTo(6);

    assertThat(game.getLetters()).containsOnlyOnce('z');
  }

  @Test
  public void testIfGameExists() {
    Game newGame = new Game("lion");
    Mockito.when(gameServ.getGameById("1")).thenReturn(newGame);

    assertThat(gameServ.getGameById("1")).isNotNull();

    assertThat(gameServ.getGameById("2")).isNull();

    assertThat(gameServ.getGameById("1")).isEqualTo(newGame);
  }
}
