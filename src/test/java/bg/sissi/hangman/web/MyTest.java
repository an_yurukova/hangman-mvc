package bg.sissi.hangman.web;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import bg.sissi.hangman.model.Game;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MyTest {
  @Spy GameService gameServ = new GameServiceImpl();

  @Test
  public void testGameCreation() {
    GameService gameServiceImpl = new GameServiceImpl();
    Game game = gameServiceImpl.startNewGame();
    assertThat(game, is(gameServiceImpl.getGameById(game.getId())));
  }
}
