package bg.sissi.hangman.web;

import static org.assertj.core.api.Assertions.assertThat;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumTest {

  WebDriver driver;
  @Spy GameService gameServ = new GameServiceImpl();
  String xPath;
  int lifesAtTheBeginning = 8;;
  int currentLifes;

  @BeforeEach
  public void setup() throws InterruptedException, URISyntaxException, IOException {

    System.setProperty(
        "webdriver.gecko.driver",
        SeleniumTest.class.getClassLoader().getResource("geckodriver.exe").getPath());

    driver = new FirefoxDriver();
    driver.get("http://localhost:8080/");
    Thread.sleep(2000);
  }

  @Test
  public void testThatGameStartsProperly() throws InterruptedException {

    startGame();

    String actualTitle = driver.getTitle();
    String expectedTitle = "Hangman";

    assertThat(actualTitle).isEqualTo(expectedTitle);
  }

  @Test
  public void testThatStartNewGameButtonIsPresent() {
    WebElement button = driver.findElement(By.xpath("//button[@class='btn btn-primary']"));

    assertThat(button).isNotNull();
    assertThat(button.getText()).isEqualTo("Start new game");
  }

  @Test
  public void whenLetterIsPresent_thenLifesAreNotDecreased() throws InterruptedException {

    String word = getWord();
    driver.navigate().back();

    Set<Character> chars =
        word.chars().mapToObj(i -> Character.valueOf((char) i)).collect(Collectors.toSet());

    for (char ch : chars) {
      makeATry(ch);

      if (driver.getTitle().equals("Hangman")) {
        currentLifes = countsLifes();
        assertThat(currentLifes).isEqualTo(8);
      }
    }

    String elementContent = driver.findElement(By.xpath("//div[@role='alert']")).getText();
    assertThat(elementContent).isEqualTo("Well done!");
  }

  @Test
  public void whenLetterIsNotPresent_thenLifesAreDecreased() throws InterruptedException {

    String word = getWord();
    driver.navigate().back();

    String lettersToCheck = getWrongLetters(word);

    Set<Character> chars =
        lettersToCheck
            .chars()
            .mapToObj(i -> Character.valueOf((char) i))
            .collect(Collectors.toSet());

    for (char ch : chars) {
      if (driver.getTitle().equals("Hangman")) {
        makeATry(ch);

        if (currentLifes > 1) {
          currentLifes = countsLifes();
          assertThat(currentLifes).isEqualTo(lifesAtTheBeginning - 1);
          currentLifes--;
        }
      }
    }

    String elementContent = driver.findElement(By.xpath("//div[@role='alert']")).getText();
    assertThat(elementContent).isEqualTo("Sorry.");
  }

  @AfterEach
  public void close() {
    driver.close();
  }

  private String startGame() throws InterruptedException {
    driver.findElement(By.xpath("//button[@class='btn btn-primary']")).click();
    Thread.sleep(2000);
    return driver.getCurrentUrl();
  }

  private String getWord() throws InterruptedException {
    String gameUrl = startGame();

    driver.navigate().to(gameUrl + "/real-word");
    Thread.sleep(2000);

    String word = driver.findElement(By.tagName("body")).getText();
    return word;
  }

  private String getWrongLetters(String word) {
    String[] alphabetAsArray =
        IntStream.range('a', 'z' + 1)
            .mapToObj(c -> Character.toString((char) c))
            .toArray(String[]::new);
    String alphabet = String.join("", alphabetAsArray);
    String wrongLetterss = alphabet.replaceAll("[" + word + "]", "");
    return wrongLetterss;
  }

  private int countsLifes() {
    String lifesAsText = driver.findElement(By.cssSelector("span")).getText();
    return Integer.parseInt(lifesAsText.substring(lifesAsText.length() - 1));
  }

  private void makeATry(char ch) throws InterruptedException {
    xPath = String.format("//input[@value='%s']", ch);
    driver.findElement(By.xpath(xPath)).click();
    Thread.sleep(2000);
  }
}
